File Shelf archives files automatically and make it inaccessible
when node (Parent entity) is unpublished.

- Problems
Most of the Drupal sites are serving files via the public download
method which means they have nearly no control over the files and
old file links will still remain accessible even the host entity
is unpublished. This becomes an issue when users click on any old
file links that cached by search engines until you remove the files
entirely from the server.

- Solutions
File Shelf offer an one-off solution by introducing a new stream
wrapper called shelf://which by default sit under the private
file system.

By enabling this module. File Shelf will take care of unpublishing /
publishing the files by changing the stream wrapper from public://
to shelf://or vice versa. Please note files under priavte:// will
be untouched.

- Field Collection
File shelf support nested field collection, it will check any file
fields that sit under the nested field collection field.

- Reusing files in multi nodes
File shelf archive file only if the file is under a unpublished node
and not referenced by other published node.

- Notes
This module works with the Workbench moderation module.
This module works with the Field collection module.
This module uses Entity Status to centralises & generalises handling
of $entity->status or $entity->published.

- How to use
1) Enable the module via /admin/modules.
2) Go to admin/config/media/file-system & setup the “Private file system path”.
2) Go to admin/config/media/file_shelf & check “Enable File Shelf”.
4) File Shelf will now start archiving file whenever you unpublished the node.
