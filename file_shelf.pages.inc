<?php

/**
 * Menu callback; prints the loaded structure of the current node/user.
 */
function file_shelf_load_files($node) {
	drupal_set_title(t('All files'));

	$fids = array();

	foreach (node_revision_list($node) as $revision) {
		$node_revision = entity_revision_load('node', $revision->vid);
		_file_shelf_get_files_by_entity($node_revision, $fids);
	}

	// Remove duplicated fids.
	$fids = array_unique($fids);

  // Build the table rows.
  $rows = array();

  foreach ($fids as $fid) {
  	$file = file_load($fid);
	  $file_uri_scheme = file_uri_scheme($file->uri);

	  if (!in_array($file_uri_scheme, array('public', 'shelf'))) {
	  	continue;
	  }

	  // File operations.
	  $file_operations = array();
	  $destination = array('query' => drupal_get_destination());

	  // Edit operation.
	  $file_operations['edit'] = l("Edit", "file/{$fid}/edit", $destination);

	  // Delete operation.
	  $file_operations['delete'] = l("Delete", "file/{$fid}/delete", $destination);

	  // Moderation operations.
	  $moderation_operations = array();

	  // Publish operation.
	  if ($file_uri_scheme === 'public') {
	  	$moderation_operations['unpublish'] = l("unpublish", "file/{$fid}/unpublish", $destination);
	  } else {
	  	$moderation_operations['publish'] = l("publish", "file/{$fid}/publish", $destination);
	  }

    $row = array(
      'data' => array(
        'fid' => $fid,
        'status' => ($file_uri_scheme === 'public') ? 'published' : 'not published',
        'title' => l($file->filename, "file/{$fid}"),
        'last_update' => format_date($file->timestamp, 'small'),
        'actions' => implode(' | ', array_filter($file_operations)),
        'moderation actions' => implode(' | ', array_filter($moderation_operations)),
      ),
      'class' => array('file'),
    );
    $rows[] = $row;
  }

  // Set the table header.
  $header = array(t('id'), t('status'), t('title'), t('last update'), t('actions'), t('moderation actions'));

  // Return properly styled output.
  $build['pager_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  return $build;
}

/**
 * Form to publish or unpublish the file.
 *
 * @param $file
 *   The file being acted upon.
 *
 * @param $action
 *   The file being performed.
 * 
 * @return
 *   A Drupal confirmation form to publish or unpublish the file.
 */
function file_shelf_file_moderation_form($form, &$form_state, $file, $action) {
	$form_state['file'] = $file;
	$form_state['action'] = $action;

	if ($action === 'publish') {
		$form['message']['#markup'] .= '<p>' . t('The file %filename will be published.', array('%filename' => $file->filename)) . '</p>';
	} else {
		$form['message']['#markup'] .= '<p>' . t('The file %filename will be unpublished.', array('%filename' => $file->filename)) . '</p>';
	}

  return confirm_form($form,
    t('Are you sure you want to %action the file %title?', array(
    	'%action' => $action,
      '%title' => entity_label('file', $file),
    )),
    'file/' . $file->fid,
    '',
    t($action)
  );
}

/**
 * Submit handler for publishing or unpublishing a file.
 */
function file_shelf_file_moderation_form_submit($form, &$form_state) {
	if ($form_state['values']['confirm']) {
		$file = $form_state['file'];

		if ($form_state['action'] === 'publish') {
			drupal_set_message(t('The file %filename is published.', array('%filename' => $file->filename)));
			_file_shelf_file_move($file, _file_shelf_shelf_to_public($file));
		}

		if ($form_state['action'] === 'unpublish') {
			drupal_set_message(t('The file %filename is unpublished.', array('%filename' => $file->filename)));
			_file_shelf_file_move($file, _file_shelf_public_to_shelf($file));
		}
	}
}
