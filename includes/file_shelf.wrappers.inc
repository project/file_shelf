<?php

/**
 * @file
 * Create shelf file stream wrapper.
 */

/**
 * Shelf (shelf://) stream wrapper class Extends DrupalLocalStreamWrapper.
 *
 * @category Class
 */
class FileShelfStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * Implements abstract public function getDirectoryPath().
   */
  public function getDirectoryPath() {
    return variable_get('file_private_path') . '/shelf';
  }

  /**
   * Implements abstract public function getExternalUrl().
   */
  public function getExternalUrl() {
    $path = str_replace('\\', '/', $this->getTarget());
    return url('system/shelf/' . $path, array('absolute' => TRUE));
  }

}
