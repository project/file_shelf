<?php

/**
 * @file
 * Provides admin interfaces and functions for file shelf.
 */

 /**
  * Retrieve the fields by entity recursively.
  *
  * @return array
  *   The file shelf admin form structure.
  */
function file_shelf_admin() {
  $form = array();
  $private_path = variable_get('file_private_path', FALSE);

  if (empty($private_path)) {
    $file_system_page = t('<a href="@file-system-page">private file path</a>', array(
      '@file-system-page' => url('admin/config/media/file-system'),
    ));

    drupal_set_message("Please setup $file_system_page before enabling the File Shelf module.", 'error');
  }

  $form['file_shelf_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable File Shelf'),
    '#default_value' => variable_get('file_shelf_enable', FALSE),
    '#description' => t("By enabling file shelf, files will be archived automatically when node unpublished."),
    '#disabled' => empty($private_path),
  );

  return system_settings_form($form);
}
